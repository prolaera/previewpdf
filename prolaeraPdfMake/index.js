const PdfPrinter = require('pdfmake/src/printer');
const fs = require('fs');
const uuid = require('uuid');

const fonts = {
  Roboto: {
    normal:
      '/opt/nodejs/node_modules/roboto-fontface/fonts/roboto/Roboto-Regular.ttf',
    bold:
      '/opt/nodejs/node_modules/roboto-fontface/fonts/roboto/Roboto-Medium.ttf',
    italics:
      '/opt/nodejs/node_modules/roboto-fontface/fonts/roboto/Roboto-RegularItalic.ttf',
    bolditalics:
      '/opt/nodejs/node_modules/roboto-fontface/fonts/roboto/Roboto-MediumItalic.ttf'
  }
};

module.exports = (document) => {
  const printer = new PdfPrinter(fonts);

  return new Promise(async (resolve, reject) => {
    try {

      const buffers = [];

      let pdfDoc;

      try {
        pdfDoc = printer.createPdfKitDocument(document);
      } catch (error) {
        throw error;
      }

      pdfDoc
        .pipe(fs.createWriteStream(`/tmp/${uuid.v4()}_output.pdf`))
        .on('error', error => {
          throw error;
        });

      pdfDoc.on('data', buffers.push.bind(buffers));

      pdfDoc.on('end', () => {
        const pdfData = Buffer.concat(buffers);

        resolve(pdfData);
      });

      pdfDoc.end();
    } catch (err) {
      reject(err);
    }
  });
};
