# Prolaera PDF Maker

This exists as a standalone package run in a Lambda Layer because of compatability issues with fonts.

## Development

### Prerequisites

- JavaScript

### Installation

```
npm install
```

### Coding style

ES6 - There's nothing crazy here.

### Testing

There are two types of tests in the project, unit and integration. Unit are deterministic to the repo, are in files named with `*.unit.test.ts`, and can be run with `npm run unit`. Integration tests reach out to ours and other dependent services, are in files named iwth `*.integration.ts` and can be run with `npm run integration`. There is additionally a `npm run test` command that will run both, along with anything with a `*.test.ts` pattern.

Unit tests are run run on commit. Commits will reject locally if the unit tests fail, and will additionally reject merges to `master` on the server.

Integration tests don't fail commits, and won't fail merges. They do need to be checked, though, especially before deployments.

General preference is unit tests over integration, but don't shy away from good integration tests when warranted.

### Git strategy

- Keep commits small and targeted
- Preface commit messages with the ID of the story
- Get at least one approval before merging
- Merge strategy is fastforward, so no merge commits

For example:

```
git checkout -b feature/myfeature
git add .
git commit -m "5551212 Updating blah blah"
git fetch
git rebase origin/master
git push origin feature/myfeature
```

### Local deployment

For local development use NPM link to link your local PDF Maker library into the API project. For instance, if you wanted to develop and test your changes in the API:

In your local API directory, run:

```
npm link ../prolaera_pdf
```

where `../prolaera_pdf` is the relative path to your local PDF Maker library repo.

## Publishing

There's no build step in this project, as we upload a zip file directly to the layer.

Run:

```
npm run zip
```

Then take the `archive.zip` file and upload it to a new version of the Lambda Layer.

If you're performing a fresh configuration of the `proUtilWorker`, add the layer to the worker in the AWS console.
