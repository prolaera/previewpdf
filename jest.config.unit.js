var config = require('./jest.config');

config.testMatch = ['**/prolaeraPdfMake/**/*.unit.test.(ts|tsx|js)'];

console.log('\x1b[33m', 'RUNNING UNIT TESTS');

module.exports = config;
