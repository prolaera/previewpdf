#!/usr/bin/env bash
rm -r nodejs
mkdir nodejs
npm ci
mv node_modules nodejs
cp -r prolaeraPdfMake nodejs/node_modules
zip -r Archive . -x "*.DS_Store" -x ".idea/*" -x ".git/*"