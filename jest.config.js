module.exports = {
  globals: {
    __DEV__: true
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'json', 'jsx'],
  modulePathIgnorePatterns: ['<rootDir>/.*/__mocks__'],
  testMatch: ['**/prolaeraPdfMake/**/*.test.(ts|tsx|js)'],
  testPathIgnorePatterns: ['/dist/', '/node_modules/'],
  testEnvironment: 'node'
};
