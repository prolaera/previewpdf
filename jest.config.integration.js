var config = require('./jest.config');

config.testMatch = ['**/prolaeraPdfMake/**/*.integration.test.(ts|tsx|js)'];

console.log('\x1b[33m', 'RUNNING INTEGRATION TESTS');

module.exports = config;
